const Configurapi = require('configurapi');
const status = require('http-status');

const awsCallbackWaitsForEmptyEventLoop = 'aws-CallbackWaitsForEmptyEventLoop';

module.exports = {
    toResponse: function(response, context)
    {
        if(response.headers && awsCallbackWaitsForEmptyEventLoop in response.headers)
        {
            context.callbackWaitsForEmptyEventLoop = response.headers[awsCallbackWaitsForEmptyEventLoop];
        }

        if(response.body instanceof Buffer)
        {
            body = response.body.toString('base64');
        }
        else if(response.body instanceof String || typeof response.body === 'string')
        {
            body = response.body;
        }
        else if(response.body instanceof Object || typeof response.body === 'object')
        {
            let jsonReplacer = 'jsonReplacer' in response ? response.jsonReplacer : undefined;

            body = JSON.stringify(response.body, jsonReplacer);
        }
        else if(response.body instanceof Number || typeof response.body === 'number')
        {
            body = response.body + '';
        } 
        else 
        {
            body = response.body;
        }

        if (context.elbProvider) {
            return {
                statusCode: response.statusCode,
                headers: response.headers,
                body: body,
                statusDescription: `${response.statusCode} ${status[response.statusCode]}`,
                isBase64Encoded: response.body instanceof Buffer,
                multiValueHeaders: Object.keys(response.headers).reduce((memo, k) => {
                    memo[k] = [response.headers[k]];
                    return memo;
                }, {})
            }
        }
        return {
            statusCode: response.statusCode,
            headers: response.headers,
            body: body,
            isBase64Encoded: response.body instanceof Buffer
        };
    },

    toRequest: function(awsRequest)
    {
        let request = new Configurapi.Request();

        request.method = (awsRequest.httpMethod || '').toLowerCase();
        request.headers = {};

        for(let headerKey of Object.keys(awsRequest.multiValueHeaders || {}))
        {
            request.headers[headerKey.toLowerCase()] = awsRequest.multiValueHeaders[headerKey].join(",");
        }
        
        for(let headerKey of Object.keys(awsRequest.headers || {}))
        {
            request.headers[headerKey.toLowerCase()] = awsRequest.headers[headerKey];
        }

        request.payload = awsRequest.isBase64Encoded ? Buffer.from(awsRequest.body, 'base64') : awsRequest.body;
        
        request.query = {};
        for(let queryKey of Object.keys(awsRequest.multiValueQueryStringParameters || {}))
        {
            let value =  awsRequest.multiValueQueryStringParameters[queryKey].length === 1 
                         ?
                         awsRequest.multiValueQueryStringParameters[queryKey][0]
                         : 
                         awsRequest.multiValueQueryStringParameters[queryKey];
                         
            request.query[queryKey.toLowerCase()] = value;
        }


        request.path = awsRequest.path || '';
        request.pathAndQuery = awsRequest.path || '';

        if(request.headers['content-type'] === 'application/json')
        {
            try
            {
                if(request.payload)
                {
                    request.payload = JSON.parse(request.payload);  
                }
            }
            catch(err)
            {
                throw err;
            } 
        }

        return request;
    }
};
