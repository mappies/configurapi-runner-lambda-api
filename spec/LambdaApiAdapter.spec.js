const chai = require('chai')
var assert = chai.assert;

const LambdaApiAdapter = require('../src/LambdaApiAdapter');

describe('Test Lambda Adapter', () => {
    it('test Multi-value ALB Proxy headers', async function () {
        let multiValueHeaders = {
            "requestContext": {
                "elb": {
                    "targetGroupArn": "arn:aws:elasticloadbalancing:us-east-1:123456789012:targetgroup/alb-test/5fe5fe5fe5fe5fe"
                }
            },
            "httpMethod": "GET",
            "path": "/hello",
            "multiValueQueryStringParameters": {
                "param1": [
                    "param1value"
                ]
            },
            "multiValueHeaders": {
                "accept": [
                    "*/*"
                ],
                "host": [
                    "alb-test-123456789012.us-east-1.elb.amazonaws.com"
                ],
                "x-forwarded-for": [
                    "2.3.4.5",
                    "5.4.3.2"
                ]
            },
            "body": "",
            "isBase64Encoded": true
        }
        let result = await LambdaApiAdapter.toRequest(multiValueHeaders)
        assert.isDefined(result.headers);
        assert.isTrue(Object.keys(result.headers).length == 3);
        assert.isTrue(result.headers['accept'] === "*/*");
        assert.isTrue(result.headers['host'] === "alb-test-123456789012.us-east-1.elb.amazonaws.com");
        assert.isTrue(result.headers['x-forwarded-for'] === "2.3.4.5,5.4.3.2");
        
    });
    it('test single-value ALB Proxy Headers', async function () {
        let singleValueHeaders = {
            "requestContext": {
                "elb": {
                    "targetGroupArn": "arn:aws:elasticloadbalancing:us-east-1:123456789012:targetgroup/alb-test/5fe5fe5fe5fe5fe"
                }
            },
            "httpMethod": "GET",
            "path": "/hello",
            "queryStringParameters": {
                "param1": "param1value"
            },
            "headers": {
                "accept": "*/*",
                "host": "alb-test-123456789012.us-east-1.elb.amazonaws.com",
                "x-forwarded-for": "2.3.4.5,5.4.3.2"
            },
            "body": "",
            "isBase64Encoded": true
        }
        let result = await LambdaApiAdapter.toRequest(singleValueHeaders)
        assert.isDefined(result.headers);
        assert.isTrue(Object.keys(result.headers).length == 3);
        assert.isTrue(result.headers['accept'] === "*/*");
        assert.isTrue(result.headers['host'] === "alb-test-123456789012.us-east-1.elb.amazonaws.com");
        assert.isTrue(result.headers['x-forwarded-for'] === "2.3.4.5,5.4.3.2");
        
    });

    it('request body is not base64encoded', async function () {
        let awsRequest = {
            "body": "test request body",
            "isBase64Encoded": false
        }
        let result = await LambdaApiAdapter.toRequest(awsRequest);
        assert.equal(result.payload.toString(), 'test request body');
    });

    it('request body is base64encoded', async function () {
        let awsRequest = {
            "body": "dGVzdCByZXF1ZXN0IGJvZHk=",
            "isBase64Encoded": true
        }
        let result = await LambdaApiAdapter.toRequest(awsRequest);
        assert.equal(result.payload.toString(), 'test request body');
    });

    it('response body is string', async function()
    {
        const eventResponse = {
            headers: {},
            statusCode: 200,
            body: "response body string"
        };
        const context = {};

        let result = await LambdaApiAdapter.toResponse(eventResponse, context);

        assert.deepEqual(result, {
            headers: {},
            statusCode: 200,
            body: 'response body string',
            isBase64Encoded: false,
        });
    });

    it('response body is buffer', async function()
    {
        let eventResponse = {
            headers: {},
            statusCode: 200,
            body: Buffer.from("response body string")
        };
        const context = {};

        let result = await LambdaApiAdapter.toResponse(eventResponse, context);

        assert.deepEqual(result, {
            headers: {},
            statusCode: 200,
            body: 'cmVzcG9uc2UgYm9keSBzdHJpbmc=',
            isBase64Encoded: true,
        });
    });
});